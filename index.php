<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html lang="pt-br">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Protec Sistemas</title>
        <link href="images/logo_protec.png" type="img/png" rel="icon"/>
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/bootstrap-reboot.min.css" rel="stylesheet" type="text/css"/>
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" /> 
        <link href="css/estilo.css" rel="stylesheet" type="text/css"/>
        <link href="css/grayscale.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/ihover.min.css" rel="stylesheet" type="text/css"/>
        <style>
            body{
                width: 100%;
                margin: 0
            }
            .module-equipe{
                text-align: center;
                background: rgba(73,155,234,1);
                background: -moz-linear-gradient(45deg, rgba(73,155,234,1) 0%, rgba(73,155,234,1) 19%, rgba(32,65,227,1) 100%);
                background: -webkit-gradient(left bottom, right top, color-stop(0%, rgba(73,155,234,1)), color-stop(19%, rgba(73,155,234,1)), color-stop(100%, rgba(32,65,227,1)));
                background: -webkit-linear-gradient(45deg, rgba(73,155,234,1) 0%, rgba(73,155,234,1) 19%, rgba(32,65,227,1) 100%);
                background: -o-linear-gradient(45deg, rgba(73,155,234,1) 0%, rgba(73,155,234,1) 19%, rgba(32,65,227,1) 100%);
                background: -ms-linear-gradient(45deg, rgba(73,155,234,1) 0%, rgba(73,155,234,1) 19%, rgba(32,65,227,1) 100%);
                background: linear-gradient(45deg, rgba(73,155,234,1) 0%, rgba(73,155,234,1) 19%, rgba(32,65,227,1) 100%);
                filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#499bea', endColorstr='#2041e3', GradientType=1 );
                max-width: 100%;
            }
/*            .logo {
                position: absolute;
                top: 0;
                left: 15px;
                height: 50px;
            }
            @media (min-width: 768px) {
                .logo {
                    height: auto; or whatever height you want it
                }
            }*/
            .testee{
                text-align: center; margin-left: 15%; width: 70%;
            }
            @media(max-width: 768px){
                .testee{
                    text-align: center; margin-left: 13%; width: 65%;
                }
            }
        </style>
    </head>
    <body>
        <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-secondary">
            <a class="navbar-brand" id="botao-teste" href="#entrada"><img class="logo img-response" style="margin-left: 20px;margin-top: -35px; margin-bottom: -15px" src="http://protecsistemas.com/image/logo.png" /></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#myNavbar" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="navbar-nav ml-auto" style="padding-right: 40px">
                    <li class="nav-item">
                        <a class="nav-link" href="#entrada">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#section-empresa">Empresa</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#section-equipe">Equipe</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#section-produto">Produtos</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#section-contato">Contato</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" data-toggle="modal" data-target="#modalAreaRestrita">Área Restrita</a>
                    </li>
                </ul>

            </div>
        </nav>
        <!-- Cabeçalho -->
        <header class="masthead" id="entrada">
            <div class="intro-body">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 mx-auto" id="botao-teste">
                            <h1 class="brand-heading">Protec Sistemas</h1>
                            <p class="lead">Tecnologia Gerando Resultados</p>
                            <a href="#section-empresa" class="btn btn-circle js-scroll-trigger">
                                <i class="fa fa-angle-down animated"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- Empresa -->
        <div class="container" id="section-empresa">
            <br/>
            <br/>
            <h1 class="display-3" style="text-align: center">Empresa</h1>
            <br/>
            <br/>
            <div class="row" style=" text-align: justify">
                <div class="col-md-12">
                    <p class="lead">A Protec Sistemas é uma empresa formada por Analistas de Sistemas, 
                        Desenvolvedores, Web Designers que possui mais de 10 anos de experiência no mercado 
                        de Tecnologia da Informação. Seu segredo para o sucesso é manter seus diversos clientes 
                        distribuídos em todo o território Nacional e Internacional com maior grau de satisfação, 
                        transparência e muita eficiência. A Protec Sistemas prima por qualidade em suas soluções, 
                        serviços e aprimoramento constante da sua equipe.</p>
                </div>
            </div>
            <br/>
            <br/>
            <div class="row justify-content-center" style="text-align: center">
                <div class="col-md-4">
                    <img src="images/mission.png"/>
                    <h5 class="display-4">Missão</h5>
                    <p class="lead" style="text-align: justify">Criar soluções para o desenvolvimento e competitividade 
                        de seus clientes, Formar grandes equipes e atender clientes no mundo todo.</p>
                </div>
                <div class="col-md-4">
                    <img src="images/eye.png"/>
                    <h5 class="display-4">Visão</h5>
                    <p class="lead" style="text-align: justify">Ser líder absoluta nas soluções oferecidas e se 
                        tornar reconhecida nacional e internacionalmente. Nossos produtos desenvolvidos vêm trazendo 
                        aos nossos clientes um controle maior sobre suas empresas.</p>
                </div>
            </div>
            <br/>
            <br/>
            <br/>
        </div>
        <!-- Equipe -->
        <div class="container module-equipe justify-content-center" style="text-align: center" id="section-equipe">
            <br/>
            <br/>
            <h1 class="display-3" style="color: #fff">Equipe</h1>
            <br/>
            <br/>
            <br/>
            <div class="row justify-content-center testee">
                <div class="col-sm-3">
                    <div class="ih-item circle effect10 bottom_to_top"><a href="#">
                            <div class="img"><img src="images/equipe/noel2.jpg" alt="img"></div>
                            <div class="info">
                                <h3>Noel Mota</h3>
                                <p>Diretor Geral</p>
                            </div></a>
                    </div>
                </div>
            </div>
            <br/>
            <br/>
            <br/>
            <div class="row justify-content-center testee">                
                <div class="col-sm-3">
                    <div class="ih-item circle effect10 bottom_to_top"><a href="#">
                            <div class="img"><img src="images/equipe/marcelo.png" alt="img"></div>
                            <div class="info">
                                <h3>Marcelo Henrique</h3>
                                <p>Gerente de Desenvolvimento</p>
                            </div></a>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="ih-item circle effect10 bottom_to_top"><a href="#">
                            <div class="img"><img src="images/equipe/nelma.png" alt="img"></div>
                            <div class="info">
                                <h3>Nelma Maria</h3>
                                <p>Gerente Financeiro</p>
                            </div></a>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="ih-item circle effect10 bottom_to_top"><a href="#">
                            <div class="img"><img src="images/equipe/nilton.png" alt="img"></div>
                            <div class="info">
                                <h3>Nilton Almeida</h3>
                                <p>Diretor Comercial</p>
                            </div></a>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="ih-item circle effect10 bottom_to_top"><a href="#">
                            <div class="img"><img src="images/equipe/viviane.jpg" alt="img"></div>
                            <div class="info">
                                <h3>Viviane Santana</h3>
                                <p>Relações Internacionais</p>
                            </div></a>
                    </div>
                </div>
            </div>
            <br/>
            <br/>
            <br/>
            <div class="row justify-content-center testee">                
                <div class="col-sm-3">
                    <div class="ih-item circle effect10 bottom_to_top"><a href="#">
                            <div class="img"><img src="images/equipe/igor.jpg" alt="img"></div>
                            <div class="info">
                                <h3>Igor Coutinho</h3>
                                <p>Desenvolvedor Mobile</p>
                            </div></a>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="ih-item circle effect10 bottom_to_top"><a href="#">
                            <div class="img"><img src="images/equipe/josimar.jpg" alt="img"></div>
                            <div class="info">
                                <h3>Josimar Cândido</h3>
                                <p>Suporte Técnico</p>
                            </div></a>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="ih-item circle effect10 bottom_to_top"><a href="#">
                            <div class="img"><img src="images/equipe/lucas.png" alt="img"></div>
                            <div class="info">
                                <h3>Lucas Andrade</h3>
                                <p>Desenvolvedor Web / Suporte Tecnico</p>
                            </div></a>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="ih-item circle effect10 bottom_to_top"><a href="#">
                            <div class="img"><img src="images/equipe/thiago.jpg" alt="img"></div>
                            <div class="info">
                                <h3>Thiago Santos</h3>
                                <p>Analista de Sistemas / Suporte Técnico</p>
                            </div></a>
                    </div>
                </div>
            </div>
            <br/>
            <br/>
            <br/>
        </div>
        <!-- Produtos-->
        <div class="container" id="section-produto" style="text-align: center">
            <br/>
            <br/>
            <h1 class="display-3 justify-content-center">Produtos</h1>
            <br/>
            <br/>
            <br/>
            <div class="row justify-content-center">
                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <div class="hovereffect">
                        <img class="img-responsive" src="images/produto_protec.png" alt="">
                        <div class="overlay">
                            <h2>4Question</h2>
                            <a class="info" href="#" data-toggle="modal" data-target="#Modal4Question">Ver mais</a>
                        </div>
                    </div>
                    <p class="lead">4Question</p>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <div class="hovereffect">
                        <img class="img-responsive" src="images/produto_protec.png" alt="">
                        <div class="overlay">
                            <h2>Orion - Gestor Funerário</h2>
                            <a class="info" href="#" data-toggle="modal" data-target="#ModalOrion">Ver mais</a>
                        </div>
                    </div>
                    <p class="lead">Orion - Gestor Funerário</p>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <div class="hovereffect">
                        <img class="img-responsive" src="images/realmed_sus.png" alt="">
                        <div class="overlay">
                            <h2>Realmed SUS</h2>
                            <a class="info" href="#" data-toggle="modal" data-target="#ModalRealmed">Ver mais</a>
                        </div>
                    </div>
                    <p class="lead">Realmed SUS</p>
                </div>
            </div>
            <br/>
            <br/>
            <br/>
        </div>
        <!-- Contatos -->
        <div class="container" id="section-contato">
            <br/>
            <br/>
            <h1 class="display-3 justify-content-center" style="text-align: center">Contato</h1>
            <br/>
            <br/>
            <br/>
            <div class="row">
                <div class="col-xl-4">
                    <form>
                        <div class="form-group">
                            <label class="control-label">Nome:</label>
                            <input type="text" class="form-control" placeholder="Nome"/>
                        </div>
                        <div class="form-group">
                            <label class="control-label">E-mail:</label>
                            <input type="email" class="form-control" placeholder="E-mail"/>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Telefone:</label>
                            <input type="tel" class="form-control telefone" placeholder="Telefone" id="telefone"/>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Mensagem:</label>
                            <textarea class="form-control" placeholder="Digite sua mensagem junto com o nome de sua Empresa e Cidade"></textarea>
                        </div>
                        <button class="btn btn-success" type="button">
                            Enviar
                        </button>
                    </form>
                </div>
                <div class="col-xl-2">

                </div>
                <br/>
                <div class="col-xl-4">
                    <p class="lead">Fale Conosco:</p>
                    <p class="lead">Email: <a href="mailto:contato@protecsistemas.com.br">contato@protecsistemas.com.br</a></p>
                    <p class="lead">Telefones:</p>
                    <ul type="none">
                        <li><i class="fa fa-phone"></i> (62)3321-7200 (Geral)</li>
                        <li><i class="fa fa-phone"></i> (62)3142-0645</li>
                    </ul>
                    <ul type="none">
                        <li><i class="fa fa-mobile"></i> (62)98127-1460 (TIM)</li>
                        <li><i class="fa fa-mobile"></i> (62)98535-5376 (OI)</li>
                        <li><i class="fa fa-mobile"></i> (62)98438-7004 (OI)</li>
                    </ul>
                </div>
            </div>
            <br/>
            <br/>
            <br/>
        </div>
        <!-- Footer -->
        <div class="container" style=" max-width: 100%; text-align: justify; background: rgba(0,0,0,0.7);">
            <br/>
            <br/>
            <br/>
            <div class="row">
                <div class="col-3"></div>
                <div class="col-3" id="map" style="height: 250px">

                </div>
                <div class="col-6"style="color: #fff">
                    <p class="lead">Protec Sistemas Processamento de Dados Ltda</p>
                    <p class="lead">Rua Galileu - Setor Bougaville - Nº 51 - 75075-570</p>
                    <p class="lead">Anápolis - Goiás - Brasil</p>
                    <p class="lead">Telefone: (62)3321-7200</p>
                </div>
            </div>
            <br/>
            <p class="lead" style="color: #fff; text-align: center">Protec &copy; 2017. Todos os Direitos Reservados</p>
            <br/>
        </div> 
        <!-- Modal 4Question -->
        <div class="modal fade" id="Modal4Question" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">4Question</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" style="text-align: justify">
                        <p class=""></p>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" data-dismiss="modal">OK</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal Orion -->
        <div class="modal fade" id="ModalOrion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Orion - Gestor Funerário</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" style="text-align: justify">
                        <div class="row">
                            <div class="col-5">

                            </div>
                            <div class="col-7">
                                <p class="lead" style="font-size: 18px">
                                    O Sistema Órion Gestor é composto por diversos módulos que são 
                                    ferramentas indispensáveis para o administrador controlar e ter parâmetros 
                                    para tomada de decisões baseadas nas variáveis fornecidas pelo sistema, 
                                    assim tornando a empresa altamente competitiva e organizada, reduzindo 
                                    sua margem de erros e maximizando resultados. Este sistema é capaz de 
                                    administrar uma única funerária ou até mesmo um grupo de empresas para 
                                    centralização da gestão.
                                </p>
                            </div>
                        </div>
                        <br/>
                        <div class="row justify-content-center">
                            <div class="col-3 col-sm-4" style="text-align: center">
                                <img src="images/Comercial.bmp" title="Módulo Comercial"/>
                                <p>Módulo Comercial</p>
                            </div>
                            <div class="col-3 col-sm-4" style="text-align: center">
                                <img src="images/Cobradores.bmp" title="Módulo Cobrança"/>
                                <p>Módulo Cobrança</p>
                            </div>
                            <div class="col-3 col-sm-4" style="text-align: center">
                                <img src="images/Estoque.bmp" title="Módulo Estoque"/>
                                <p>Módulo Estoque</p>
                            </div>
                        </div>
                        <br/>
                        <div class="row justify-content-center">
                            <div class="col-3 col-sm-4" style="text-align: center">
                                <img src="images/Financeiro.bmp" title="Módulo Financeiro"/>
                                <p>Módulo Financeiro</p>
                            </div>
                            <div class="col-3 col-sm-4" style="text-align: center">
                                <img src="images/Servicos.bmp" title="Módulo Serviços"/>
                                <p>Módulo Serviços</p>
                            </div>
                            <div class="col-3 col-sm-4" style="text-align: center">
                                <img src="images/Saude.bmp" title="Realmed"/>
                                <p>Realmed</p>
                            </div>                            
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" data-dismiss="modal">OK</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal Realmed SUS -->
        <div class="modal fade" id="ModalRealmed" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-md" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Realmed SUS</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" style="text-align: center">
                        <img src="images/LogoRealmedLogin.png" class="img-responsive" width="300" height="auto" title="Logo Realmed SUS"/>
                        <br/>
                        <br/>
                        <p class="lead" style="text-align: justify">
                            O Realmed SUS é um sistema desenvolvido para facilitar o preenchimento 
                            das fichas de cadastro domicilar e melhor o atendimento nas unidades básicas de saúde 
                            da região norte do País.
                        </p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" data-dismiss="modal">OK</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal Area Restrita -->
        <div class="modal fade" id="modalAreaRestrita" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Área Restrita</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" style="text-align: justify">
                        <form method="post">
                            <div class="form-group col-xl-12">
                                <label class="control-label">Login:</label>
                                <input class="form-control" type="text"/>
                            </div>
                            <div class="form-group col-xl-12">
                                <label class="control-label">Senha:</label>
                                <input class="form-control" type="password"/>
                            </div>
                            <div class="form-group col-xl-12" style="text-align: right">
                                <a href=""><button class="btn btn-success">Entrar</button></a>
                                <a href=""><button class="btn btn-dark" data-dismiss="modal">Cancelar</button></a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>


        <!-- Scripts -->
        <script src="js/jquery.min.js" type="text/javascript"></script>
        <script src="js/jquery.mask.js" type="text/javascript"></script>
        <script type="text/javascript">
            jQuery("#telefone").mask("(99)9999-9999");
        </script>
        <script>
            function initMap() {
                var protec = {lat: -16.296952, lng: -48.939610};
                var map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 18,
                    center: protec,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    styles: [
                        {
                            "stylers": [{"visibility": "on"}, {"saturation": -100},
                                {"gamma": 0.54}]
                        },
                        {
                            "featureType": "road",
                            "elementType": "labels.icon",
                            "stylers": [{"visibility": "off"}]
                        },
                        {
                            "featureType": "water",
                            "stylers": [{"color": "#4d4946"}]
                        },
                        {
                            "featureType": "poi",
                            "elementType": "labels.icon",
                            "stylers": [{"visibility": "off"}]
                        },
                        {
                            "featureType": "poi",
                            "elementType": "labels.text",
                            "stylers": [{"visibility": "simplified"}]
                        },
                        {
                            "featureType": "road",
                            "elementType": "geometry.fill",
                            "stylers": [{"color": "#ffffff"}]
                        },
                        {
                            "featureType": "road.local",
                            "elementType": "labels.text",
                            "stylers": [{"visibility": "simplified"}]
                        },
                        {
                            "featureType": "water",
                            "elementType": "labels.text.fill",
                            "stylers": [{"color": "#ffffff"}]
                        },
                        {
                            "featureType": "transit.line",
                            "elementType": "geometry",
                            "stylers": [{"gamma": 0.48}]
                        },
                        {
                            "featureType": "transit.station",
                            "elementType": "labels.icon",
                            "stylers": [{"visibility": "off"}]
                        },
                        {
                            "featureType": "road",
                            "elementType": "geometry.stroke",
                            "stylers": [{"gamma": 7.18}]
                        }
                    ]
                });

                //Imagem do marcador
                var image = 'https://cdn2.iconfinder.com/data/icons/bitsies/128/Location-48.png';

                //Corpo da janela de informação
                var contentString = '<div id="content">' +
                        '<div id="siteNotice">' +
                        '</div>' +
                        '<div id="bodyContent">' +
                        '<p><b>Protec Sistemas</b><br/>Tecnologia Gerando Resultados</p>' +
                        '</div>' +
                        '</div>';

                //Chamada do corpo de informação
                var infowindow = new google.maps.InfoWindow({
                    content: contentString
                });

                //Marcador do mapa
                var marker = new google.maps.Marker({
                    position: protec,
                    map: map,
                    icon: image,
                    title: 'Protec Sistemas',
                    animation: google.maps.Animation.BOUNCE
                });

                //Evento de Click para informação
                marker.addListener('click', function () {
                    infowindow.open(map, marker);
                });
            }
        </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDjg3pWupa8s8wfJ497BaeeVqOsctdkJ8E&callback=initMap"></script>
        <script>
            $(document).ready(function () {
                // Add scrollspy to <body>
                $('body').scrollspy({target: ".navbar", offset: 90});

                // Add smooth scrolling on all links inside the navbar
                $("#myNavbar a").on('click', function (event) {
                    // Make sure this.hash has a value before overriding default behavior
                    if (this.hash !== "") {
                        // Prevent default anchor click behavior
                        event.preventDefault();

                        // Store hash
                        var hash = this.hash;

                        // Using jQuery's animate() method to add smooth page scroll
                        // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
                        $('html, body').animate({
                            scrollTop: $(hash).offset().top
                        }, 800, function () {

                            // Add hash (#) to URL when done scrolling (default click behavior)
                            window.location.hash = hash;
                        });
                    }  // End if
                });

                $("#botao-teste").on('click', function (event) {
                    if (this.hash !== "") {
                        // Prevent default anchor click behavior
                        event.preventDefault();

                        // Store hash
                        var hash = this.hash;

                        // Using jQuery's animate() method to add smooth page scroll
                        // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
                        $('html, body').animate({
                            scrollTop: $(hash).offset().top
                        }, 800, function () {

                            // Add hash (#) to URL when done scrolling (default click behavior)
                            window.location.hash = hash;
                        });
                    }
                });
            });
        </script>
    </body>
</html>
